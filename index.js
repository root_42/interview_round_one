const puppeteer = require('puppeteer');

async function cssSelector() {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://prefeitura.pbh.gov.br/saude/licitacao/pregao-eletronico-151-2020');

  bidding = '#block-views-block-view-noticia-pbh-block-5 > div > div > div > div > div > div.views-field.views-field-nothing > span > span:nth-child(19)';
  await page.waitForSelector(bidding);
  let bidding_element = await page.$(bidding);
  let bidding_result = await bidding_element.evaluate(el => el.textContent, bidding_element);

  console.log(bidding_result);
  publication = '#block-views-block-view-noticia-pbh-block-5 > div > div > div > div > div > div.views-field.views-field-nothing > span > span:nth-child(1)';
  await page.waitForSelector(publication);
  let publication_element = await page.$(publication);
  let publication_date = await publication_element.evaluate(el => el.textContent,publication_element);
  console.log(publication_date);
  obj = '#block-views-block-view-noticia-pbh-block-5 > div > div > div > div > div > div.views-field.views-field-nothing > span > p:nth-child(6)';
  await page.waitForSelector(obj);
  let object_element = await page.$(obj);
  let object_result = await object_element.evaluate(el => el.textContent, object_element);
  console.log(object_result);
}
cssSelector();
